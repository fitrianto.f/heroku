from signal import signal, SIGINT
from os import path as ospath, remove as osremove, execl as osexecl
from subprocess import run as srun, check_output
from psutil import *
from time import time
from sys import executable
from telegram.ext import CommandHandler
from bot import *
from platform import machine
from .helper.ext_utils.fs_utils import start_cleanup, clean_all, exit_clean_up
from .helper.ext_utils.bot_utils import get_readable_file_size, get_readable_time, progress_bar
from .helper.ext_utils.db_handler import DbManger
from .helper.telegram_helper.bot_commands import BotCommands
from .helper.telegram_helper.message_utils import sendMessage, sendMarkup, editMessage, sendLogFile
from .helper.telegram_helper.filters import CustomFilters
from .helper.telegram_helper.button_build import ButtonMaker
from .modules import authorize, list, cancel_mirror, mirror_status, mirror_leech, clone, ytdlp, shell, eval, delete, count, leech_settings, search, rss, bt_select, sleep

def stats(update, context):
    total, used, free, disk = disk_usage('/')
    swap = swap_memory()
    memory = virtual_memory()
    cpu_u = cpu_percent(interval=0.5)
    mem_p = memory.percent
    swap_p = swap.percent
    if ospath.exists('.git'):
        last_commit = check_output(["git log -1 --date=short --pretty=format:'%cd <b>\nFrom</b>: %cr'"], shell=True).decode()
        botVersion = check_output(["git log -1 --date=format:v%y.%m%d.%H%M --pretty=format:%cd"], shell=True).decode()
    else:
        last_commit = 'No UPSTREAM_REPO'
        botVersion = 'No UPSTREAM_REPO'
    stats = f'<b>Last Commit</b>: {last_commit}\n\n'\
            f'<b>Bot Uptime</b>: {get_readable_time(time() - botStartTime)}\n'\
            f'<b>OS Uptime</b>: {get_readable_time(time() - boot_time())}\n'\
            f'<b>OS Type</b>: {machine()}\n\n'\
            f'<b>Total Disk Space </b>: {get_readable_file_size(total)}\n'\
            f'<b>Used</b>: {get_readable_file_size(used)} | <b>Free</b>: {get_readable_file_size(free)}\n\n'\
            f'<b>Upload</b>: {get_readable_file_size(net_io_counters().bytes_sent)}\n'\
            f'<b>Download</b>: {get_readable_file_size(net_io_counters().bytes_recv)}\n\n'\
            f'<code>CPU  : {progress_bar(cpu_u)} {cpu_u}%</code>\n'\
            f'<code>RAM  : {progress_bar(mem_p)} {mem_p}%</code>\n'\
            f'<code>DISK : {progress_bar(disk)} {disk}%</code>\n'\
            f'<code>SWAP : {progress_bar(swap_p)} {swap_p}%</code>\n\n'\
            f'<b>Physical Cores</b>: {cpu_count(logical=False)} | '\
            f'<b>Total</b>: {cpu_count(logical=True)}\n\n'\
            f'<b>SWAP</b>: {get_readable_file_size(swap.total)}\n'\
            f'<b>Memory Total</b>: {get_readable_file_size(memory.total)}\n'\
            f'<b>Free</b>: {get_readable_file_size(memory.available)}'\
            f'<b> | Used</b>: {get_readable_file_size(memory.used)}\n'
    sendMessage(stats, context.bot, update.message)


def start(update, context):
    message = update.message
    if message.from_user.username:
        tag = f"@{message.from_user.username}"
    else:
        tag = message.from_user.mention_html(message.from_user.first_name)
    if CustomFilters.authorized_user(update) or CustomFilters.authorized_chat(update):
        if update.message.chat.type == "private":
            sendMessage(f"<b>Congratulations, {tag}\nAm All Yours.</b>", context.bot, update.message)
        else:
            sendMessage(f"<b>Hey {tag},\nThis chat is authorised.</b>", context.bot, update.message)
    else:
        start__ = f"Hey {tag},\nYour can't use me here.\nJoin Us - @KuyShareID ❤️"
        sendMessage(start__, context.bot, update.message)

def restart(update, context):
    restart_message = sendMessage("Restarting...", context.bot, update.message)
    if Interval:
        Interval[0].cancel()
        Interval.clear()
    alive.kill()
    clean_all()
    srun(["pkill", "-9", "-f", "gunicorn|chrome|firefox|megasdkrest|opera"])
    srun(["python3", "update.py"])
    with open(".restartmsg", "w") as f:
        f.truncate(0)
        f.write(f"{restart_message.chat.id}\n{restart_message.message_id}\n")
    osexecl(executable, executable, "-m", "bot")

def limits(update, context):
    uname = update.effective_user.first_name

    torrent_limit = 'Empty' if TORRENT_DIRECT_LIMIT == '' else f'{TORRENT_DIRECT_LIMIT}GB/Link'
    clone_limit = 'Empty' if CLONE_LIMIT == '' else f'{CLONE_LIMIT}GB/Link'
    mega_limit = 'Empty' if MEGA_LIMIT == '' else f'{MEGA_LIMIT}GB/Link'
    storage_thre_limit = 'Empty' if STORAGE_THRESHOLD == '' else f'{STORAGE_THRESHOLD}GB/Free'
    zip_unzip_limit = 'Empty' if ZIP_UNZIP_LIMIT == '' else f'{ZIP_UNZIP_LIMIT}GB/Link'
    leech_limit = 'Empty' if LEECH_LIMIT == '' else f'{LEECH_LIMIT}GB/Link'
    ttask = 'Empty' if TOTAL_TASKS_LIMIT == '' else f'{TOTAL_TASKS_LIMIT} Tasks/Time'
    utask = 'Empty' if USER_TASKS_LIMIT == '' else f'{USER_TASKS_LIMIT} Tasks/Users'

    limits = f'<b><u>Bot Limitations:</u></b>\n'\
             f'<b>Torrent/Direct: </b>{torrent_limit}\n'\
             f'<b>Zip/Unzip: </b>{zip_unzip_limit}\n'\
             f'<b>Clone: </b>{clone_limit}\n'\
             f'<b>Leech: </b>{leech_limit}\n'\
             f'<b>Mega: </b>{mega_limit}\n'\
             f'<b>Storage Threshold: </b>{storage_thre_limit}\n\n'\
             f'<b>Total Tasks: </b>{ttask}\n'\
             f'<b>User Tasks: </b>{utask}\n'

    reply_message = sendMessage(f"{uname}, Getting Limits....Wait.", context.bot, update.message)
    editMessage(f'{limits}', reply_message)  

def ping(update, context):
    start_time = int(round(time() * 1000))
    reply = sendMessage("Measuring...", context.bot, update.message)
    end_time = int(round(time() * 1000))
    ping_ = f'<b>Pong: </b>{end_time - start_time} ms \n'
    ping_ += f"<b>Uptime: </b>{get_readable_time(time() - botStartTime)}\n" 
    editMessage(f'{ping_}', reply)

def log(update, context):
    sendLogFile(context.bot, update.message)

help_string = f'''
NOTE: Try each command without any perfix to see more detalis.

/{BotCommands.MirrorCommand[0]} or /{BotCommands.MirrorCommand[1]}: Start mirroring to Google Drive.

/{BotCommands.ZipMirrorCommand[0]} or /{BotCommands.ZipMirrorCommand[1]}: Start mirroring and upload the file/folder compressed with zip extension.

/{BotCommands.UnzipMirrorCommand[0]} or /{BotCommands.UnzipMirrorCommand[1]}: Start mirroring and upload the file/folder extracted from any archive extension.

/{BotCommands.QbMirrorCommand[0]} or /{BotCommands.QbMirrorCommand[1]}: Start Mirroring to Google Drive using qBittorrent.

/{BotCommands.QbZipMirrorCommand[0]} or /{BotCommands.QbZipMirrorCommand[1]}: Start mirroring using qBittorrent and upload the file/folder compressed with zip extension.

/{BotCommands.QbUnzipMirrorCommand[0]} or /{BotCommands.QbUnzipMirrorCommand[1]}: Start mirroring using qBittorrent and upload the file/folder extracted from any archive extension.

/{BotCommands.YtdlCommand[0]} or /{BotCommands.YtdlCommand[1]}: Mirror yt-dlp supported link.

/{BotCommands.YtdlZipCommand[0]} or /{BotCommands.YtdlZipCommand[1]}: Mirror yt-dlp supported link as zip.

/{BotCommands.LeechCommand[0]} or /{BotCommands.LeechCommand[1]}: Start leeching to Telegram.

/{BotCommands.ZipLeechCommand[0]} or /{BotCommands.ZipLeechCommand[1]}: Start leeching and upload the file/folder compressed with zip extension.

/{BotCommands.UnzipLeechCommand[0]} or /{BotCommands.UnzipLeechCommand[1]}: Start leeching and upload the file/folder extracted from any archive extension.

/{BotCommands.QbLeechCommand[0]} or /{BotCommands.QbLeechCommand[1]}: Start leeching using qBittorrent.

/{BotCommands.QbZipLeechCommand[0]} or /{BotCommands.QbZipLeechCommand[1]}: Start leeching using qBittorrent and upload the file/folder compressed with zip extension.

/{BotCommands.QbUnzipLeechCommand[0]} or /{BotCommands.QbUnzipLeechCommand[1]}: Start leeching using qBittorrent and upload the file/folder extracted from any archive extension.

/{BotCommands.YtdlLeechCommand[0]} or /{BotCommands.YtdlLeechCommand[1]}: Leech yt-dlp supported link.

/{BotCommands.YtdlZipLeechCommand[0]} or /{BotCommands.YtdlZipLeechCommand[1]}: Leech yt-dlp supported link as zip.

/{BotCommands.CloneCommand} [drive_url]: Copy file/folder to Google Drive.

/{BotCommands.CountCommand} [drive_url]: Count file/folder of Google Drive.

/{BotCommands.DeleteCommand} [drive_url]: Delete file/folder from Google Drive (Only Owner & Sudo).

/{BotCommands.LeechSetCommand} [query]: Leech settings.

/{BotCommands.SetThumbCommand}: Reply photo to set it as Thumbnail.

/{BotCommands.BtSelectCommand}: Select files from torrents by gid or reply.

/{BotCommands.RssListCommand[0]} or /{BotCommands.RssListCommand[1]}: List all subscribed rss feed info (Only Owner & Sudo).

/{BotCommands.RssGetCommand[0]} or /{BotCommands.RssGetCommand[1]}: Force fetch last N links (Only Owner & Sudo).

/{BotCommands.RssSubCommand[0]} or /{BotCommands.RssSubCommand[1]}: Subscribe new rss feed (Only Owner & Sudo).

/{BotCommands.RssUnSubCommand[0]} or /{BotCommands.RssUnSubCommand[1]}: Unubscribe rss feed by title (Only Owner & Sudo).

/{BotCommands.RssSettingsCommand[0]} or /{BotCommands.RssSettingsCommand[1]} [query]: Rss Settings (Only Owner & Sudo).

/{BotCommands.CancelMirror}: Cancel task by gid or reply.

/{BotCommands.CancelAllCommand} [query]: Cancel all [status] tasks.

/{BotCommands.ListCommand} [query]: Search in Google Drive(s).

/{BotCommands.SearchCommand} [query]: Search for torrents with API.

/{BotCommands.StatusCommand}: Shows a status of all the downloads.

/{BotCommands.StatsCommand}: Show stats of the machine where the bot is hosted in.

/{BotCommands.PingCommand}: Check how long it takes to Ping the Bot (Only Owner & Sudo).

/{BotCommands.AuthorizeCommand}: Authorize a chat or a user to use the bot (Only Owner & Sudo).

/{BotCommands.UnAuthorizeCommand}: Unauthorize a chat or a user to use the bot (Only Owner & Sudo).

/{BotCommands.AuthorizedUsersCommand}: Show authorized users (Only Owner & Sudo).

/{BotCommands.AddSudoCommand}: Add sudo user (Only Owner).

/{BotCommands.RmSudoCommand}: Remove sudo users (Only Owner).

/{BotCommands.RestartCommand}: Restart and update the bot (Only Owner & Sudo).

/{BotCommands.SleepCommand}: idle the bot (Only Owner & Sudo).

/{BotCommands.LogCommand}: Get a log file of the bot. Handy for getting crash reports (Only Owner & Sudo).

/{BotCommands.ShellCommand}: Run shell commands (Only Owner).

/{BotCommands.EvalCommand}: Run Python Code Line | Lines (Only Owner).

/{BotCommands.ExecCommand}: Run Commands In Exec (Only Owner).

/{BotCommands.ClearLocalsCommand}: Clear {BotCommands.EvalCommand} or {BotCommands.ExecCommand} locals (Only Owner).
'''

def bot_help(update, context):
    sendMessage(help_string, context.bot, update.message)

def main():
    if ospath.exists('.git'):
        commit_msg = commit_messages = check_output(['git', 'log', '-1', '--format=%s']).decode('utf8')
        botVersion = check_output(["git log -1 --date=format:v%y.%m%d.%H%M --pretty=format:%cd"], shell=True).decode()
    else:
        commit_msg = 'No UPSTREAM_REPO'
        botVersion = 'No UPSTREAM_REPO'
    start_cleanup()
    notifier_dict = False
    if INCOMPLETE_TASK_NOTIFIER and DB_URI is not None:
        if notifier_dict := DbManger().get_incomplete_tasks():
            for cid, data in notifier_dict.items():
                if ospath.isfile(".restartmsg"):
                    with open(".restartmsg") as f:
                        chat_id, msg_id = map(int, f)
                    msg = "#Restarted" \
                          f"\n<b>Version: </b>{botVersion}" \
                          f"\n<b>Changelog:</b>\n{commit_msg}"
                else:
                    msg = "#Restarted" \
                          f"\n<b>Version: </b>{botVersion}" \
                          f"\n<b>Changelog:</b>\n{commit_msg}" \
                          f"\n\n<b>Mirror your links again:</b>"  
                for tag, links in data.items():
                     msg += f"\n{tag}: "
                     for index, link in enumerate(links, start=1):
                         msg += f" <a href='{link}'>{index}</a> |"
                         if len(msg.encode()) > 4000:
                             if 'Restarted Successfully!' in msg and cid == chat_id:
                                 bot.editMessageText(msg, chat_id, msg_id, parse_mode='HTML', disable_web_page_preview=True)
                                 osremove(".restartmsg")
                             else:
                                 try:
                                     bot.sendMessage(cid, msg, 'HTML', disable_web_page_preview=True)
                                 except Exception as e:
                                     LOGGER.error(e)
                             msg = ''
                if 'Restarted Successfully!' in msg and cid == chat_id:
                     bot.editMessageText(msg, chat_id, msg_id, parse_mode='HTML', disable_web_page_preview=True)
                     osremove(".restartmsg")
                else:
                    try:
                        bot.sendMessage(cid, msg, 'HTML', disable_web_page_preview=True)
                    except Exception as e:
                        LOGGER.error(e)

    if ospath.isfile(".restartmsg"):
        with open(".restartmsg") as f:
            chat_id, msg_id = map(int, f)
        bot.edit_message_text("Restarted Successfully!", chat_id, msg_id)
        osremove(".restartmsg")
    elif not notifier_dict and AUTHORIZED_CHATS:
        for id_ in AUTHORIZED_CHATS:
            try:
                bot.sendMessage(id_, "Bot Restarted!", 'HTML')
            except Exception as e:
                LOGGER.error(e)

    start_handler = CommandHandler(BotCommands.StartCommand, start, run_async=True)
    ping_handler = CommandHandler(BotCommands.PingCommand, ping,
                                  filters=CustomFilters.authorized_chat | CustomFilters.authorized_user, run_async=True)
    limits_handler = CommandHandler(BotCommands.LimitsCommand, limits,
                                  filters=CustomFilters.authorized_chat | CustomFilters.authorized_user, run_async=True) 
    restart_handler = CommandHandler(BotCommands.RestartCommand, restart,
                                     filters=CustomFilters.owner_filter | CustomFilters.sudo_user, run_async=True)
    help_handler = CommandHandler(BotCommands.HelpCommand,
                                  bot_help, filters=CustomFilters.authorized_chat | CustomFilters.authorized_user, run_async=True)
    stats_handler = CommandHandler(BotCommands.StatsCommand,
                                   stats, filters=CustomFilters.authorized_chat | CustomFilters.authorized_user, run_async=True)
    log_handler = CommandHandler(BotCommands.LogCommand, log, filters=CustomFilters.owner_filter | CustomFilters.sudo_user, run_async=True)
    dispatcher.add_handler(start_handler)
    dispatcher.add_handler(ping_handler)
    dispatcher.add_handler(limits_handler)
    dispatcher.add_handler(restart_handler)
    dispatcher.add_handler(help_handler)
    dispatcher.add_handler(stats_handler)
    dispatcher.add_handler(log_handler)
    updater.start_polling(drop_pending_updates=IGNORE_PENDING_REQUESTS)
    LOGGER.info("Congratulations, Bot Started Sucessfully !")
    signal(SIGINT, exit_clean_up)

app.start()
main()
if USER_SESSION_STRING:
    app_session.run()
else:
    pass
main_loop.run_forever()
