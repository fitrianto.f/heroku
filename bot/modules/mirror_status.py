import requests
from psutil import cpu_percent, virtual_memory, disk_usage
from time import time
from threading import Thread
from telegram.ext import CommandHandler, CallbackQueryHandler
from bot import *
from bot.helper.telegram_helper.message_utils import sendMessage, deleteMessage, auto_delete_message, sendStatusMessage, editMessage, update_all_messages
from bot.helper.ext_utils.bot_utils import get_readable_file_size, get_readable_time, turn, setInterval
from bot.helper.telegram_helper.filters import CustomFilters
from bot.helper.telegram_helper.bot_commands import BotCommands


def mirror_status(update, context):
    url = 'https://api.quotable.io/random'
    r = requests.get(url)
    quote = r.json()
    quote_ = (quote['content'])
    quote_a = (quote['author'])
    with download_dict_lock:
        count = len(download_dict)
    if count == 0:
        currentTime = get_readable_time(time() - botStartTime)
        free = get_readable_file_size(disk_usage(DOWNLOAD_DIR).free)
        message = f'No active tasks!\n\n'
        message += f"<b>Bot Quote:</b>\n<code>{quote_}</code>\n"
        message += f"<b>~</b> <code>{quote_a}</code>"
        message += f"\n\n<b>UPTIME:</b> {currentTime} | <b>FREE:</b> {free}"
        reply_message = sendMessage(f"Getting Status...", context.bot, update.message)
        editMessage(f'{message}', reply_message)
        Thread(target=auto_delete_message, args=(context.bot, update.message, reply_message)).start()
    else:
        index = update.effective_chat.id
        with status_reply_dict_lock:
            if index in status_reply_dict:
                deleteMessage(context.bot, status_reply_dict[index][0])
                del status_reply_dict[index]
            try:
                if Interval:
                    Interval[0].cancel()
                    Interval.clear()
            except:
                pass
            finally:
                Interval.append(setInterval(DOWNLOAD_STATUS_UPDATE_INTERVAL, update_all_messages))
        sendStatusMessage(update.message, context.bot)
        deleteMessage(context.bot, update.message)

def status_pages(update, context):
    query = update.callback_query
    query.answer()
    data = query.data
    data = data.split()
    if data[1] == "ref":
        update_all_messages(True)
        return
    done = turn(data)
    if not done:
        query.message.delete()


mirror_status_handler = CommandHandler(BotCommands.StatusCommand, mirror_status,
                                       filters=CustomFilters.authorized_chat | CustomFilters.authorized_user, run_async=True)

status_pages_handler = CallbackQueryHandler(status_pages, pattern="status", run_async=True)
dispatcher.add_handler(mirror_status_handler)
dispatcher.add_handler(status_pages_handler)
